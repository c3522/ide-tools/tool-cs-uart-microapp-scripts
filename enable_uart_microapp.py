#!/usr/bin/env python3

import time
from crownstone_uart.core.CrownstoneUart  import CrownstoneUart
from crownstone_uart.core.UartEventBus import UartEventBus
from crownstone_uart.topics.UartTopics import UartTopics

uart = CrownstoneUart()

# Start up the USB bridge.
uart.initialize_usb_sync()
# you can alternatively do this async by
# await uart.initialize_usb()

# Function that's called when the power usage is updated.
def showUartMessage(data):
    print("Received payload", data)

# Set up event listeners
UartEventBus.subscribe(UartTopics.uartMessage, showUartMessage)

# the try except part is just to catch a control+c, time.sleep does not appreciate being killed.
try:
    print("Sending validation cmd via UART...")
    if(uart._usbDev.validate_microapp(0)):
        print("Command send")
    else:
        print("Command didn't send")
        exit

    time.sleep(0.2)

    print("Sending enable cmd via UART...")
    if(uart._usbDev.enable_microapp(0)):
        print("Command send")
    else:
        print("Command didn't send")
        exit

except KeyboardInterrupt:
    print("Closing example.... Thanks for your time!")

uart.stop()

